<?php

use mf\router\Router;
use barmiton\view\BarmitonView;
use barmiton\auth\BarmitonAuthentification;

$loader = new \Composer\Autoload\ClassLoader();
$loader->register('src');

session_start();

/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

$config = parse_ini_file ('conf/config.ini');

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

BarmitonView::addStyleSheet("https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.css");
BarmitonView::setAppTitle("Barmiton");

$auth = new BarmitonAuthentification();

$router = new Router();

$router->addRoute('home',
    '/home/',
    '\barmiton\control\BarmitonController',
    'viewHome',
    BarmitonAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('ViewNewCocktail',
    '/ViewNewCocktail/',
    '\barmiton\control\BarmitonController',
    'viewNewCocktail',
    BarmitonAuthentification::ACCESS_LEVEL_NONE);

$router->setDefaultRoute('/home/');

try {
    $router->run();
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
}














