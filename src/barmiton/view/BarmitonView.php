<?php

namespace barmiton\view;

use \mf\view\AbstractView;
use mf\router\Router;
use barmiton\auth\BarmitonAuthentification;


class BarmitonView extends AbstractView {

    public function __construct( $data ){
        parent::__construct($data);
    }

    private function renderHeader(){
        return '

<ul id="dropdown1" class="dropdown-content">
    <li><a href="#!">Mes Favoris</a></li>
    <li><a href="#!">Mes Recettes</a></li>
    <li><a href="#!">Nouvelle Recette</a></li>
    <li class="divider"></li>
    <li><a href="#!">Mon Compte</a></li>
  </ul>

  <ul id="dropdown2" class="dropdown-content">
    <li><a href="#!"></a>Sans Gluten</a></li>
    <li><a href="#!"></a>Sans Gluten</a></li>
  </ul>
                <nav class="navbar-fixed">
                    <div class="nav-wrapper">
                      <a href="#" class="brand-logo right">Barminton</a>
                      <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">User<i class="material-icons right">arrow_drop_down</i></a></li>
                        <li><a href="badges.html">Admin</a></li>
                      </ul>
                    </div>
                  </nav>';
    }

    private function renderMenuBottom(){

    }

    private function renderFooter(){
        return '<footer class="page-footer">
    <div class="container">
      <div class="row">
        <div class="col l2 offset-l10 s12">
          <img class="materialboxed" width="100" src="http://pluspng.com/img-png/cocktail-png-hd-cocktail-png-clipart-cocktail-png-600.png">
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        © 2014 Copyright Text
        <a class="grey-text text-lighten-4 right" href="#!">Société Pajaritos</a>
      </div>
    </div>
  </footer>';
    }

    /* Affiche tous les Tweets dans Home.*/
    private function renderHome(){

    }

    protected function renderLogin(){
        $res =
            "<div id='renderLogin'>"
            . "<h2> Connexion à votre compte </h2><br>"
            ."<form action=" .Router::urlFor('loginSend', []) ." method='post'>"
            . $this->checkError()
            . "<p> Username </p>"
            . "<input
                                type='text'
                                name='username'
                                placeholder='username'> <br>"
            . "<p> Mot de passe </p>"
            . "<input
                                type='password'
                                name='password'
                                placeholder='mot de passe'> <br>"
            . "<input
                                 type='submit'
                                 name='connexionCompte'
                                 value='Me connecter'
                                 id='connexionCompte'> <br>"
            ."</form>"
            ."<div>"
            ."<a href=" .Router::urlFor('signup', []) .">"
            ."<input
                             type='submit'
                             name='creerCompte'
                             value='Créer mon compte'
                             id='creerCompte'> </a>"
            ."</div>"
            ."</div>";

        return $res;
    }

    public function renderSignup() {
        $res =
            "<div id='renderSignup'>"
            . "<h2> Créez votre compte </h2><br>"
            ."<form action=" .Router::urlFor('checkSignup', []) ." method='post'>"
            . $this->checkError()
            . "<p> Nom Complet </p>"
            . "<input
                                type='text'
                                name='fullname'
                                placeholder='Prénom Nom'> <br>"
            . "<p> Username </p>"
            . "<input
                                type='text'
                                name='username'
                                placeholder='username'> <br>"
            . "<p> Mot de passe </p>"
            . "<input
                               type='password'
                               name='password'
                               placeholder='mot de passe'> <br>"
            . "<p> Confirmer le mot de passe </p>"
            . "<input
                               type='password'
                               name='passwordConfirm'
                               placeholder='retapez votre mdp'> <br>"
            . "<input
                               type='submit'
                               name='connexionCompte'
                               value='Valider'
                               id='creerCompteForm'> <br>"
            ."</form>"
            ."</div>";

        return $res;
    }

    protected function renderBody($selector=null){

        $header = $this->renderHeader();
        $body=' <div id="BodyTweet">';
        switch ($selector) {
            case "renderHome":
                $body .= $this->renderHome();
                break;


            case "renderLogin":
                $body .= $this->renderLogin();
                break;

            case "renderNewCocktail" :
                $body .= $this->renderNewCocktail();
                break;

            case "renderSignup":
                $body .= $this->renderSignup();
                break;

        }
        $body.='</div>';
        $footer = $this->renderFooter();

        $html = <<<EOT
<section>
    {$header}
    {$body}
    {$footer}
</section>
EOT;
        return $html;

    }

    private function renderNewCocktail(){
        $html = '<div class="col s12">
                    <ul class="tabs tabs-fixed-width">
                      <li class="tab col s3"><a href="#test1">Nouvelle Recette</a></li>
                      <li class="tab col s3"><a href="#test2">Mes recettes</a></li>
                      <li class="tab col s3"><a href="#test3"></a>Mes favoris</li>
                      <li class="tab col s3"><a href="#test4">Mon Compte</a></li>
                    </ul>
                  </div>';

        $html .= '<div class="row">
    <div id="test1" class="container centered col s12" style="height: auto;">
      <div class="row">
        <h1 class="centered">Nouveau cocktail</h1>
      </div>
      <!--row-->

      <div class="row">
        <div class="col s12 m8 l8">
          <div class="row">
            <div class="col s2 m2 l2">
              <h7>Nom du cocktail</h7>
            </div>
            <div class="col s10 m10 l10">
              <input type="text" placeholder="Nom du cocktail">
            </div>
          </div>
        </div>
        <div class="col s12 m4 l4">
          <div class="row">
            <div class="col s2 m2 l2"> <span>temps</span>
            </div>
            <div class="col s10 m10 l10"> <input type="number" placeholder="min" maxlength="3">
            </div>
          </div>
        </div>
      </div> <!-- row -->

      <div class="row">
        <div class="col s12 m8 l8">
          <h7>Spécificité(s)</h7>
          <a class="dropdown-trigger" href="#!" data-target="dropdown1">Sans gluten<i class="material-icons right">arrow_drop_down</i></a>

        </div>
        <div class="col s12 m4 l4">
          <div class="row">
            <div class="col s2 m2 l2">
              <span>% alcool</span>
            </div>
            <div class="col s10 m10 l10">
              <input type="text" placeholder="%" maxlength="3">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col s12 m8 l8">
          <h7>Moment(s)</h7>
          <a class="dropdown-trigger" href="#!" data-target="dropdown1">Été, soir<i class="material-icons right">arrow_drop_down</i></a>
        </div>

        <div class="col s12 m4 l4">
          <div class="row">
            <div class="col s2 m2 l2"> <span>Prix</span>
            </div>
            <div class="col s10 m10 l10"> <input type="text" placeholder="Prix" maxlength="3">

            </div>
          </div>
        </div>

      </div> <!-- row-->

      <hr>

      <div class="row">
        <div class="col s12 m6 l6">
          <h8>Liste d\'ingredients</h8>
          <input type="text" placeholder="Ingredient 1">
          <input type="text" placeholder="Ingredient 2">
          <input type="text" placeholder="Ingredient 3">
          <a><i class="material-icons centered">add</i></a>
        </div>

        <div class="col s12 m6 l6">
          <h8>Les étapes</h8>
          <ol>
            <li><input type="text" placeholder="Step 1"></li>
            <li><input type="text" placeholder="Step 2"></li>
            <a><i class="material-icons centered">add</i></a>
          </ol>
        </div>
      </div>

      <div class="row">
        <div class="col offset-s5">
          <a class="waves-effect waves-light btn-large">Valider</a>
        </div>
      </div>
    </div>

    <div id="test2">
      <p>Larry Cagnonga</p>
    </div>

    <div id="test3">
      <p>QTKBX</p>
    </div>

    <div id="test4">
      <p>TUPAN8N</p>
    </div>

  </div>';
    }


}
