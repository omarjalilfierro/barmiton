<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 04/11/2018
 * Time: 19:04
 */

namespace barmiton\control;

use mf\auth\Authentification;
use mf\control\AbstractController;
use barmiton\auth\BarmitonAuthentification;
use barmiton\view\BarmitonView;
use \mf\utils\HttpRequest;
use mf\router\Router;


class BarmitonAdminController extends AbstractController
{
    public function viewLogin($error=null) {
        if($error){
            $v = new BarmitonView(['error' => $error]);
        } else {
            $v = new BarmitonView([]);
        }
        return $v->render("renderLogin");
    }

    public function checklogin() {
        $req = new HttpRequest();
        //var_dump($req);

        $auth = new BarmitonAuthentification();
        $username = $req->post['username'];
        $password = $req->post['password'];
        try {
            if($this->validateWithFilter($username) && $this->validateWithFilter($password)){
                $auth->loginUser($username, $password);
                if(!empty($_SESSION['user_login'])){
                    $tc = new BarmitonController();
                    if($_SESSION['access_level'] == BarmitonAuthentification::ACCESS_LEVEL_USER){
                        $tc->viewFollower();
                    } else {
                        $tc->viewDashBoardFollower();
                    }
                    //$auteur = User::where('username', '=', $username)->first();
                    //$listFollowers = $auteur->followedBy()->get();
                }
            }
            //$v = new BarmitonView($listFollowers);
            //return $v->render("renderFollowers");
        } catch (\Exception $e){
            $this->viewLogin($e->getMessage());
        }
    }

    public function logout(){
        $auth = new Authentification();
        $auth->logout();

        header('Location:' .Router::urlFor('maison', []));
        // ou return $v->render("renderHome"); mais du coup on aurait pas la
        //bonne url (ce serait l'url logout avec le rendu de home.
    }

    public function signup($error=null){
        if($error){
            $v = new BarmitonView(['error' => $error]);
        }else{
            $v = new BarmitonView([]);
        }
        return $v->render("renderSignup");
    }

    public function checkSignup(){

        $req = new HttpRequest();
        $fullname = $req->post['fullname'];
        $username = $req->post['username'];
        $password = $req->post['password'];
        $passwordConfirm = $req->post['passwordConfirm'];

       try {
            if($password == $passwordConfirm){
                if($this->validateWithFilter($fullname)
                    && $this->validateWithFilter($username)
                    && $this->validateWithFilter($password)){

                    $ta = new BarmitonAuthentification();
                    $ta->createUser($username, $password, $fullname);
                    $this->checklogin();
                }
            } else {
                throw new \Exception("La confirmation du mot de passe est incorrecte");
            }
        } catch (\Exception $e){
           $this->signup($e->getMessage());
        }

    }

    public function validateWithFilter($var){
        if (filter_var ($var, FILTER_UNSAFE_RAW)){
            return true;
        }else{
            throw new \Exception("texte non valide");
            return false;
        }
    }

}